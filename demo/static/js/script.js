!(function () {

    "use strict";

    // 获取 json
    let json = null;
    function getJSON () {
        return new Promise(resolve => {
            if ( json ) {
                resolve( json );
            } else {
                $.getJSON( "static/json/area.json" ).then(res => {
                    json = res;
                    resolve( res );
                })
            }
        })
    }

    // 打印选择结果
    function log ( el, code, name, map ) {
        const _code = `[ ${ code.join( ", " ) } ]`;
        const _name = `[ ${ name.join( ", " ) } ]`;
        const content = `
            <p class="log-title">code:</p>
            <p>${ _code }</p>
            <p class="log-title">name:</p>
            <p>${ _name }</p>
            <p class="log-title">all:</p>
            <p><pre>${ JSON.stringify( map, null, 2 ) }</pre></p>
        `;
        $( el ).next().html( content.trim() );
    }

    // 宽度
    const selectWidth = 131;
    const cascaderPanelWidth = "100%";

    // select 主题
    !(function selectTheme () {
        iPicker("#select-target-01", {
            theme: "select",
            data: {
                source: getJSON()
            },
            width: selectWidth,
            onSelect ( code, name, map ) {
                log( "#select-target-01", code, name, map );
            }
        });
        iPicker("#select-target-02", {
            theme: "select",
            data: {
                source: getJSON()
            },
            width: selectWidth,
            icon: "arrow-outline",
            onSelect ( code, name, map ) {
                log( "#select-target-02", code, name, map );
            }
        });
        iPicker("#select-target-03", {
            theme: "select",
            data: {
                source: getJSON()
            },
            width: 201,
            level: 2,
            onSelect ( code, name, map ) {
                log( "#select-target-03", code, name, map );
            }
        });
        const select04 = iPicker("#select-target-04", {
            theme: "select",
            data: {
                source: getJSON()
            },
            width: selectWidth,
            disabled: [ 2, 3 ],
            onSelect ( code, name, map ) {
                log( "#select-target-04", code, name, map );
            }
        });
        $( "#selectDisabledBtn" ).on("click", function () { 
            iPicker.disabled( select04, [ 2, 3 ] );
        })
        $( "#selectEnabledBtn" ).on("click", function () {
            iPicker.enabled( select04, [ 2, 3 ] );
        })
        iPicker("#select-target-05", {
            theme: "select",
            data: {
                source: getJSON()
            },
            width: selectWidth,
            selected: [ "230000", "230100", "230103" ],
            onSelect ( code, name, map ) {
                log( "#select-target-05", code, name, map );
            }
        });
        iPicker("#select-target-06", {
            theme: "select",
            data: {
                source: getJSON()
            },
            width: selectWidth,
            strict: true,
            onSelect ( code, name, map ) {
                log( "#select-target-06", code, name, map );
            }
        });
        iPicker("#select-target-07", {
            theme: "select",
            data: {
                source: getJSON()
            },
            width: selectWidth,
            radius: 100,
            onSelect ( code, name, map ) {
                log( "#select-target-07", code, name, map );
            }
        });
        iPicker("#select-target-08", {
            theme: "select",
            data: {
                source: getJSON()
            },
            width: selectWidth,
            maxHeight: 150,
            onSelect ( code, name, map ) {
                log( "#select-target-08", code, name, map );
            }
        });
        const select09 = iPicker("#select-target-09", {
            theme: "select",
            data: {
                source: getJSON()
            },
            width: selectWidth,
            clearable: true,
            onClear () {
                const code = iPicker.get( select09, "code" );
                const name = iPicker.get( select09, "name" );
                const all = iPicker.get( select09, "all" );
                if ( code.length ) {
                    log( "#select-target-09", code, name, all );
                } else {
                    $( "#select-target-09" ).next().empty();
                }
            },
            onSelect ( code, name, map ) {
                log( "#select-target-09", code, name, map );
            }
        });
        iPicker("#select-target-10", {
            theme: "select",
            data: {
                source: getJSON()
            },
            width: selectWidth,
            disabledItem: [ "110000", "130000", "210100", "210602" ],
            onSelect ( code, name, map ) {
                log( "#select-target-10", code, name, map );
            }
        });
    })();

    // cascader 主题
    !(function cascaderTheme () {
        iPicker("#cascader-target-01", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            onSelect ( code, name, map ) {
                log( "#cascader-target-01", code, name, map );
            }
        });
        iPicker("#cascader-target-02", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            icon: "arrow-outline",
            onSelect ( code, name, map ) {
                log( "#cascader-target-02", code, name, map );
            }
        });
        iPicker("#cascader-target-03", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            level: 2,
            onSelect ( code, name, map ) {
                log( "#cascader-target-03", code, name, map );
            }
        });
        const cascader04 = iPicker("#cascader-target-04", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            disabled: [ 1 ],
            onSelect ( code, name, map ) {
                log( "#cascader-target-04", code, name, map );
            }
        });
        $( "#cascaderDisabledBtn" ).on("click", function () { 
            iPicker.disabled( cascader04, [ 1 ] );
        })
        $( "#cascaderEnabledBtn" ).on("click", function () {
            iPicker.enabled( cascader04, [ 1 ] );
        })
        iPicker("#cascader-target-05", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            selected: [ "230000", "230100", "230103" ],
            onSelect ( code, name, map ) {
                log( "#cascader-target-05", code, name, map );
            }
        });
        iPicker("#cascader-target-06", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            strict: true,
            onSelect ( code, name, map ) {
                log( "#cascader-target-06", code, name, map );
            }
        });
        iPicker("#cascader-target-07", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            radius: 100,
            onSelect ( code, name, map ) {
                log( "#cascader-target-07", code, name, map );
            }
        });
        iPicker("#cascader-target-08", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            maxHeight: 150,
            onSelect ( code, name, map ) {
                log( "#cascader-target-08", code, name, map );
            }
        });
        iPicker("#cascader-target-09", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            clearable: true,
            onClear () {
                $( "#cascader-target-09" ).next().empty();
            },
            onSelect ( code, name, map ) {
                log( "#cascader-target-09", code, name, map );
            }
        });
        iPicker("#cascader-target-10", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            disabledItem: [ "110000", "130000", "210100", "210602" ],
            onSelect ( code, name, map ) {
                log( "#cascader-target-10", code, name, map );
            }
        });
        iPicker("#cascader-target-50", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            separator: "-",
            onSelect ( code, name, map ) {
                log( "#cascader-target-50", code, name, map );
            }
        });
        iPicker("#cascader-target-51", {
            theme: "cascader",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            onlyShowLastLevel: true,
            onSelect ( code, name, map ) {
                log( "#cascader-target-51", code, name, map );
            }
        });
    })();

    // panel 主题
    !(function panelTheme () {
        iPicker("#panel-target-01", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            onSelect ( code, name, map ) {
                log( "#panel-target-01", code, name, map );
            }
        });
        iPicker("#panel-target-02", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            icon: "arrow-outline",
            onSelect ( code, name, map ) {
                log( "#panel-target-02", code, name, map );
            }
        });
        iPicker("#panel-target-03", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            level: 2,
            onSelect ( code, name, map ) {
                log( "#panel-target-03", code, name, map );
            }
        });
        const panel04 = iPicker("#panel-target-04", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            disabled: [ 1 ],
            onSelect ( code, name, map ) {
                log( "#panel-target-04", code, name, map );
            }
        });
        $( "#panelDisabledBtn" ).on("click", function () { 
            iPicker.disabled( panel04, [ 1 ] );
        })
        $( "#panelEnabledBtn" ).on("click", function () {
            iPicker.enabled( panel04, [ 1 ] );
        })
        iPicker("#panel-target-05", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            selected: [ "230000", "230100", "230103" ],
            onSelect ( code, name, map ) {
                log( "#panel-target-05", code, name, map );
            }
        });
        iPicker("#panel-target-06", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            strict: true,
            onSelect ( code, name, map ) {
                log( "#panel-target-06", code, name, map );
            }
        });
        iPicker("#panel-target-07", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            radius: 100,
            onSelect ( code, name, map ) {
                log( "#panel-target-07", code, name, map );
            }
        });
        iPicker("#panel-target-08", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            maxHeight: 150,
            onSelect ( code, name, map ) {
                log( "#panel-target-08", code, name, map );
            }
        });
        iPicker("#panel-target-09", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            clearable: true,
            onClear () {
                $( "#panel-target-09" ).next().empty();
            },
            onSelect ( code, name, map ) {
                log( "#panel-target-09", code, name, map );
            }
        });
        iPicker("#panel-target-10", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            disabledItem: [ "110000", "130000", "210100", "210602" ],
            onSelect ( code, name, map ) {
                log( "#panel-target-10", code, name, map );
            }
        });
        iPicker("#panel-target-50", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            separator: "|",
            onSelect ( code, name, map ) {
                log( "#panel-target-50", code, name, map );
            }
        });
        iPicker("#panel-target-51", {
            theme: "panel",
            data: {
                source: getJSON()
            },
            width: cascaderPanelWidth,
            onlyShowLastLevel: true,
            onSelect ( code, name, map ) {
                log( "#panel-target-51", code, name, map );
            }
        });
    })();

    $( "#params-btn" ).click(function () {
        $( this ).find( "i" ).toggle();
        $( "#params" ).toggle().prev().toggle();
    })

    // 组件方法
    function clearLog () {
        $( "#method-cascader + .pre" ).empty(); 
    }
    const method = {
        create () {
            clearLog();
            const picker = iPicker("#method-cascader", {
                theme: "cascader",
                data: {
                    source: getJSON()
                },
                width: "100%",
                disabled: true,
                clearable: true,
                onClear () {
                    clearLog();
                },
                onSelect ( code, name, map ) {
                    log( "#method-cascader", code, name, map );
                }
            });
            return picker;
        },
        destroy () {
            clearLog();
            iPicker.destroy( IPicker );
        },
        get () {
            window.alert( iPicker.get( IPicker ) );
        },
        set () {
            iPicker.set( IPicker, [ "230000", "230100", "230103" ] );
        },
        disabled () {
            iPicker.disabled( IPicker, true );
        },
        enabled () {
            iPicker.enabled( IPicker, true );
        },
        clear () {
            clearLog();
            iPicker.clear( IPicker );
        },
        reset () {
            clearLog();
            return iPicker.reset( IPicker );
        }
    };
    let IPicker = method.create();
    $( ".main.method" ).on("click", "button", function () {
        const type = $( this ).data( "type" );
        if ( type === "create" || type === "reset" ) {
            IPicker = method[ type ]();
        } else {
            method[ type ]();
        }
    })

    const duration = 350;

    // 关闭弹窗 ( 公共方法 )
    $( "#container" ).on("click", ".public-layer-title i", function () {
        $( this ).parent().parent().slideUp( duration ).parent().fadeOut( duration );
    })

    // 打开弹窗 ( 公共方法 )
    $( "header" ).on("click", "button", function () {
        $( `#layer-${ $( this ).data( "type" ) }` ).fadeIn( duration ).find( ".public-layer-wrapper" ).slideDown( duration );
    })

    // 点击标题跳转到代码仓库
    $( "header b" ).on("click", function () {
        location.href = "https://gitee.com/dreamer365/iPicker";
    })

    // 跳转到项目列表
    $( "button[data-type='other']" ).click(function () {
        $( this ).next().get( 0 ).click();
    })
    
})();